\chapter{Résultats obtenus}

Dans cette partie, nous allons détailler et analyser les résultats obtenus grâce au programme de 
simulation.

\section{Instance 01}

Dans le tableau suivant, l’instance utilisée par nos variantes est la première, 
nommée \textbf{instance01.dat}.

Chaque ligne de valeurs correspond à un algorithme particulier, nommé en en-tête de ligne.
Et chaque colonne correspond à une configuration de placement précise, nommé en en-tête de colonne. 
Les noms de chaque colonne peuvent sembler un peu complexe à première vue mais ils suivent tous le
format suivant : 

Le mot présent avant \textbf{CH} définit le placement de l’hôtel de ville :

\begin{itemize}
    \item « Default », l’hôtel de ville sera placé sur le coin supérieur gauche du terrain.
    \item « Centered », l’hôtel de ville sera placé au centre du terrain.
    \item « Random », l’hôtel de ville sera placé aléatoirement dans le terrain.
\end{itemize}

La suite de mots située après \textbf{CH} définit comment les placements valides sont triés :

\begin{itemize}
    \item « WithoutSort », aucun tri, premier placement valide trouvé, premier placement utilisé.
    \item « WithAscendingSort », le placement valide choisi en priorité sera le plus proche de 
          l’hôtel de ville.
    \item « WithDescendingSort », le placement valide choisi en priorité sera le plus éloigné de 
          l’hôtel de ville.
    \item « WithRandomSort », le placement valide choisi est complétement aléatoire.
\end{itemize}

Chaque cellule correspond au score de la variante d’algorithme en question.

Pour rappel, le score de l’algorithme correspond à la somme des aires des bâtiments placés dans 
le terrain.
Si ce score est décimal, cela signifie simplement qu’il s’agit d’une moyenne de score : 
cet algorithme a été exécuté plusieurs fois.

Dans ce tableau, nous pouvons également voir que certaines cellules contiennent un nombre décimal 
entre parenthèse.
Il s’agit de l’écart-type, le fait qu’un écart-type soit précisé signifie encore une fois 
que l’algorithme correspondant a été exécuté plusieurs fois.

Voici un premier tableau avec tous les scores ainsi que les écarts-types pour \textbf{l’instance 01} :

\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{images/instance01_full.png}
    \caption{Instance 01}
\end{figure}

La variante de tableau ci-dessous reprend les mêmes résultats, en ignorant certaines configurations 
inutiles (score faible), mais en mettant en évidence les meilleurs scores obtenus.

\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{images/instance01_eval.png}
    \caption{Evaluation instance 01}
\end{figure}

Ce tableau est très utile afin de déterminer les algorithmes les plus performants : 
ici par exemple, le meilleur score obtenu est égal à 86.

L’algorithme utilisé pour atteindre ce score est le \textbf{MaxArea}, et voici sa configuration : 
\textbf{defaultCHWithDescendingSort}.

\begin{itemize}
    \item Hôtel de ville placé en haut à gauche ;
    \item Placement en priorité le plus loin possible de l’hôtel de ville.
\end{itemize}

On remarque également que toutes les variantes d’algorithme \textbf{MaxArea} obtiennent des scores 
bien plus élevés que la moyenne. Les variantes de \textbf{MinArea}, quant à elles, sont bien moins 
performantes, quelle que soit la configuration de placement associée.

En ce qui concerne les écarts-types de variantes aléatoires, on remarque que les algorithmes 
\textbf{MinArea} et \textbf{MinSpace} sont plutôt stables.

\section{Instance 02}

Passons maintenant à l’instance n°2 :

Encore une fois, on retrouve deux tableaux différents. Le premier donne tous les scores et 
écarts-types pour chaque variante : 

\begin{figure}[h]
    \includegraphics[width=\linewidth]{images/instance02_full.png}
    \caption{Instance 02}
\end{figure}

Le second se concentre sur les scores, avec mise en évidence des meilleurs : 

\begin{figure}[h]
    \includegraphics[width=\linewidth]{images/instance02_eval.png}
    \caption{Evaluation instance 02}
\end{figure}

On remarque tout d’abord que le meilleur score obtenu sur cette instance est égal à 91.
La configuration est identique à la meilleure configuration de l’instance 01.
Cependant, il ne s’agit pas du même algorithme.

Le meilleur algorithme s’appelle \textbf{Engine} : l’algorithme par défaut est donc le plus 
performant. Ce dernier n’effectue aucun tri sur les bâtiments, il se contente de les placer dans 
l’ordre donné par l’instance en question. En d’autres termes, l’ordre des bâtiments donné par 
cette instance est optimal, il n’est pas nécessaire de le trier avant placement.

Autre chose, on remarque que les algorithmes \textbf{MaxArea} et \textbf{MaxSpace} se distinguent des
autres : quelle que soit la configuration, ils ont tendance tous les deux à fournir des
scores élevés.

De plus, la configuration \textbf{CenteredCHWithDescendingSort} a tendance également à fournir des
scores plus élevés que la moyenne, quel que soit l’algorithme utilisé.

Néanmoins, certaines configurations ont l’effet inverse : par exemple, la configuration 
\textbf{defaultCHWithoutSort} fait tendre tous les résultats vers le bas, quel que soit
l’algorithme utilisé.

En ce qui concerne les écart-types, ils sont réguliers pour la plupart des variantes, mais 
\textbf{MaxArea} et \textbf{MaxSpace} se distinguent avec, en moyenne, des écarts-types plus faibles 
que les autres.

\section{Instance 03}

Passons maintenant désormais à l’instance 03.

\begin{figure}[h]
    \includegraphics[width=\linewidth]{images/instance03_full.png}
    \caption{Instance 03}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=\linewidth]{images/instance03_eval.png}
    \caption{Evaluation instance 03}
\end{figure}

Premièrement, on remarque que le meilleur score atteint est égal à 379.
Ensuite, en ce qui concerne la variante utilisée :

\begin{itemize}
    \item Le meilleur algorithme est identique au meilleur algorithme de l’instance 01 
          (\textbf{MaxArea}).
    \item La configuration, quant à elle, est identique à la meilleure configuration de l’instance 02 
          (\textbf{centeredCHWithDescendingSort}).
\end{itemize}

Ainsi, la meilleure variante pour cette instance est la combinaison du meilleur algorithme d’une 
instance, avec la meilleure configuration d’une autre instance. Encore une fois, les algorithmes
\textbf{MaxArea} et \textbf{MaxSpace} se distinguent par des scores généralement plus élevés 
que la moyenne.

En ce qui concerne l’écart-type des algorithmes aléatoires, on remarque premièrement qu’ils sont 
bien plus élevés qu’auparavant.

Cela s'explique par le fait que l’instance 03 est plus conséquente, que ce soit en termes de 
dimensions de terrain ou en nombre de bâtiments pouvant être placés. 
Cela explique les scores élevés que tous nos algorithmes
peuvent atteindre. En effet, le terrain de cette instance possède les dimensions suivantes :  
20 x 20, alors que les instance 01 et 02 possèdent un terrain de dimension 10 x 10.

Lorsque que nos algorithmes impliquent de l’aléatoire, les scores sont bien plus irréguliers
car les bâtiments placés sont souvent différents d’une exécution à une autre.
En ce qui concerne les valeurs d’écarts-types, \textbf{MinSpace} semble être un peu plus régulier 
que les autres.

\section{Instance 04}

Passons maintenant à l'instance 04.

\begin{figure}[h]
    \includegraphics[width=\linewidth]{images/instance04_full.png}
    \caption{Instance 04}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=\linewidth]{images/instance04_eval.png}
    \caption{Evaluation instance 04}
\end{figure}

Tout d’abord, on remarque que le meilleur score atteint pour cette instance est égal à 205.
Le meilleur algorithme utilisé est le \textbf{MaxSpace}.
La meilleure configuration de placement est la suivante : \textbf{centeredCHWithDescendingSort}.

\begin{itemize}
    \item Hôtel de ville placé au centre du terrain ;
    \item Placement valide utilisé en priorité loin de l’hôtel de ville.
\end{itemize}

On peut déjà remarquer que cette configuration de placement fait toujours tendre les scores vers le 
haut, quels que soient l’algorithme et l’instance utilisés.
On remarque également que \textbf{MaxArea} est très performant pour cette instance.
Les scores en question sont très proches de 202, voire atteignent cette valeur.

En sachant que l’un des scores les plus élevés provient d’une configuration de placement aléatoire :

On parle de la configuration : « \textbf{randomCHWithDescendingSort} ». 
Ici, l’hôtel de ville est placé aléatoirement.

L’algorithme \textbf{MaxArea} couplé à cette configuration donne une moyenne de score valant 202.73, 
avec un écart-type très correct (5.09).
Une moyenne aussi haute signifie certainement que cette variante est capable d’obtenir un score 
encore plus élevé, supérieur à 205.

Dernier détail que l’on peut relever ici, parmi toutes les configurations testées, un paramètre de 
configuration sort souvent du lot : on remarque que les configurations dotées de l’option 
\textbf{WithDescendingSort} sont bien plus performants que les autres configurations.
Cela signifie que les variantes dont les placements utilisés sont, en priorité, les plus éloignés 
de l’hôtel de ville, sont également les plus performants.

En ce qui concerne les écarts-types, ils sont stables pour la plupart mais le \textbf{MinArea} est 
légèrement plus régulier que les autres avec une configuration aléatoire.

\section{Instance 05}

Passons désormais à la dernière instance : l'instance 05.

\begin{figure}[h]
    \includegraphics[width=\linewidth]{images/instance05_full.png}
    \caption{Instance 05}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=\linewidth]{images/instance05_eval.png}
    \caption{Evaluation instance 05}
\end{figure}

En ce qui concerne le meilleur score, on remarque qu’il est atteint par les variantes
\textbf{MaxArea} et \textbf{MaxSpace}, couplés à une configuration 
\textbf{centeredCHWithDescendingSort}.
Le score en question est égal à 208.

On remarque également que les couples d’algorithmes (MaxArea – MaxSpace) et (MinArea – MinSpace) 
non aléatoires fournissent toujours les mêmes scores.
Cela signifie certainement que les bâtiments de ces instances sont toujours triés dans le même ordre,
que ce soit selon leurs encombrements ou leurs aires.

De plus, encore une fois, la configuration \textbf{centeredCHWithDescendingSort} se distingue des 
autres avec des scores plus élevés.
Même chose pour les algorithmes \textbf{MaxArea} et \textbf{MaxSpace} qui fournissent généralement 
de meilleurs scores que les autres algorithmes présents.

En ce qui concerne les écarts-types, \textbf{MinArea} et \textbf{MinSpace} aléatoires fournissent 
généralement des résultats plus réguliers que les autres.

\section{Conclusion de l'analyse}

Nous allons revenir sur les performances des différentes variantes dans un contexte plus général, 
en prenant en compte l’ensemble des instances testées.

Pour commencer, les algorithmes \textbf{MinArea} et \textbf{MinSpace} font généralement partie des 
algorithmes les moins performants en termes de score.
Néanmoins, ces mêmes algorithmes sont également les plus réguliers lorsqu’ils sont couplés à 
une configuration de placement aléatoire.

En ce qui concerne le meilleur algorithme, à partir de nos résultats précédents, il s’agit 
certainement de l’algorithme \textbf{MaxArea}, qui est responsable du meilleur score atteint pour
3 instances sur 5 testées.

Pour la meilleure configuration de placement, on peut déduire qu’il s’agit de la suivante :
\textbf{CenteredCHWithDescendingSort}.
Elle est responsable du meilleur score pour 4 instances sur 5.

Finalement, la combinaison gagnante, adaptée à quasiment toutes les instances, est la 
combinaison \textbf{MaxArea – centeredCHWithDescendingSort}.
C’est d’ailleurs cette combinaison qui est actuellement mise en place par défaut dans nos programmes 
\textbf{Main} et \textbf{MainFX} (programmes faisant partie du package executables).

Cette combinaison n’offre certes pas le meilleur score pour toutes les instances, mais, à partir de 
nos tests, il semblerait que le score atteint soit toujours optimal.
En effet, lorsque le score atteint n’est pas le meilleur, il en reste très proche. 
(D’après nos tests, à 3 points près exactement).

Si on considère désormais cette variante comme un algorithme à part entière, on peut en déduire 
qu’il s’agit de l’un des plus performants, mais également l’un des plus réguliers.
N’oublions pas que ce dernier ne repose pas sur de l’aléatoire.
