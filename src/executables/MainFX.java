package executables;

import core.Engine;
import core.algos.MaxArea;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ui.BuildingPane;
import ui.EvaluationBar;
import utilities.FileReader;
import utilities.PlacementConfiguration;

public class MainFX extends Application
{
    private static final int WIDTH = 1200;
    private static final int HEIGHT = 1200;

    private Scene scene;
    private BorderPane root;
    private BuildingPane pane;
    private EvaluationBar evaluationBar;

    @Override
    public void start(Stage stage)
    {
        // INPUT Actuel -> Meilleur configuration
        // Pour plus d'informations concernant les input disponibles, Voir le fichier suivant : inputDetails.txt

        // INPUT 1
        FileReader fr = new FileReader("instances/instance03.dat");

        // INPUT 2
        PlacementConfiguration placementConfiguration = new PlacementConfiguration("middle", "distance", 1);

        // INPUT 3
        Engine engine = new MaxArea(fr, placementConfiguration);

        root = new BorderPane();
        scene = new Scene(root, WIDTH,HEIGHT+38);

        root.getStylesheets().add(MainFX.class.getResource("../css/Buildings.css").toExternalForm());

        pane = new BuildingPane(HEIGHT, engine);
        root.setCenter(pane);

        evaluationBar = new EvaluationBar(engine);
        root.setBottom(evaluationBar);

        stage.setTitle("Buildings");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}