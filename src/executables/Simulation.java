package executables;

import core.Engine;
import core.algos.*;
import utilities.FileReader;
import utilities.PlacementConfiguration;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class Simulation {
    // Liste de toutes les variantes de configuration possibles.
    private static final PlacementConfiguration defaultCHWithoutSort = new PlacementConfiguration("default", "default", 0);
    private static final PlacementConfiguration defaultCHWithAscendingSort = new PlacementConfiguration("default", "distance", -1);
    private static final PlacementConfiguration defaultCHWithDescendingSort = new PlacementConfiguration("default", "distance", 1);
    private static final PlacementConfiguration defaultCHWithRandomSort = new PlacementConfiguration("default", "random", 0);

    private static final PlacementConfiguration centeredCHWithoutSort = new PlacementConfiguration("middle", "default", 0);
    private static final PlacementConfiguration centeredCHWithAscendingSort = new PlacementConfiguration("middle", "distance", -1);
    private static final PlacementConfiguration centeredCHWithDescendingSort = new PlacementConfiguration("middle", "distance", 1);
    private static final PlacementConfiguration centeredCHWithRandomSort = new PlacementConfiguration("middle", "random", 0);

    private static final PlacementConfiguration randomCHWithoutSort = new PlacementConfiguration("random", "default", 0);
    private static final PlacementConfiguration randomCHWithAscendingSort = new PlacementConfiguration("random", "distance", -1);
    private static final PlacementConfiguration randomCHWithDescendingSort = new PlacementConfiguration("random", "distance", 1);
    private static final PlacementConfiguration randomCHWithRandomSort = new PlacementConfiguration("random", "random", 0);

    /** Nombre de simulations à effectuer lorsqu'un algorithme repose sur de l'aléatoire. */
    private static final int nbSimulationForRandom = 100;

    /** Utilitaire de formatage (utilisé pour l'écart-type). */
    private static final NumberFormat formatter = new DecimalFormat("#0.00", DecimalFormatSymbols.getInstance(Locale.US));

    private static final ArrayList<String> variantNames = new ArrayList<>(Arrays.asList(
            "defaultCHWithoutSort","defaultCHWithAscendingSort","defaultCHWithDescendingSort","defaultCHWithRandomSort",
            "centeredCHWithoutSort","centeredCHWithAscendingSort","centeredCHWithDescendingSort","centeredCHWithRandomSort",
            "randomCHWithoutSort","randomCHWithAscendingSort","randomCHWithDescendingSort","randomCHWithRandomSort"));

    public static void main(String[] args) {
        // Pour chaque instance...
        for(int i = 0 ; i < 5 ; i++){
            try {
                // Récupération des paramètres d'instances et préparation pour nos simulations.
                String instanceNameDat = "instances/instance0"+ (i + 1) + ".dat";
                String instanceNameCsv = "instances/instance0"+ (i + 1) + ".csv";
                FileReader fr = new FileReader(instanceNameDat);
                File myObj = new File(instanceNameCsv);
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                } else {
                    if(myObj.delete())
                        if(!myObj.createNewFile()){
                            System.out.println("An error occurred.");
                            return;
                        }
                }
                FileWriter writer = new FileWriter(instanceNameCsv);
                writer.write("variant," + variantNames.toString().replaceAll("[\\p{Ps}\\p{Pe}]", ""));

                // Lancement des simulations.
                computeAlgorithm(writer, new Engine(fr), instanceNameCsv);
                computeAlgorithm(writer, new MinArea(fr), instanceNameCsv);
                computeAlgorithm(writer, new MaxArea(fr), instanceNameCsv);
                computeAlgorithm(writer, new MinSpace(fr), instanceNameCsv);
                computeAlgorithm(writer, new MaxSpace(fr), instanceNameCsv);
                computeAlgorithm(writer, new Randomizer(fr), instanceNameCsv);

                writer.close();

            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
    }

    /** Cette méthode permet de lancer un ensemble de simulation sur un type d'algorithme particulier.
     *
     * @param writer Instance de FileWriter pour écrire dans un fichier nos résultats.
     * @param algorithm Instance de l'algorithme à utiliser.
     * @param instanceName Nom du type de l'algorithme.
     *
     * @throws IOException Exception en cas de problème d'ouverture/lecture/écriture de fichier.
     */
    private static void computeAlgorithm(FileWriter writer, Engine algorithm, String instanceName) throws IOException {
        boolean randomizer = false;

        if(algorithm instanceof Randomizer)
            randomizer = true;

        // Lancement des simulations pour chaque variante.
        writer.write("\n" + algorithm.getClass().getSimpleName() + ",");
        writer.write(computeSimulation(algorithm, defaultCHWithoutSort, randomizer, instanceName, variantNames.get(0)) + ",");
        writer.write(computeSimulation(algorithm, defaultCHWithAscendingSort, randomizer, instanceName, variantNames.get(1)) + ",");
        writer.write(computeSimulation(algorithm, defaultCHWithDescendingSort, randomizer, instanceName, variantNames.get(2)) + ",");
        writer.write(computeSimulation(algorithm, defaultCHWithRandomSort, true, instanceName, variantNames.get(3)) + ",");

        writer.write(computeSimulation(algorithm, centeredCHWithoutSort, randomizer, instanceName, variantNames.get(4)) + ",");
        writer.write(computeSimulation(algorithm, centeredCHWithAscendingSort, randomizer, instanceName, variantNames.get(5)) + ",");
        writer.write(computeSimulation(algorithm, centeredCHWithDescendingSort, randomizer, instanceName, variantNames.get(6)) + ",");
        writer.write(computeSimulation(algorithm, centeredCHWithRandomSort, true, instanceName, variantNames.get(7)) + ",");

        writer.write(computeSimulation(algorithm, randomCHWithoutSort, true, instanceName, variantNames.get(8)) + ",");
        writer.write(computeSimulation(algorithm, randomCHWithAscendingSort, true, instanceName, variantNames.get(9)) + ",");
        writer.write(computeSimulation(algorithm, randomCHWithDescendingSort, true, instanceName, variantNames.get(10)) + ",");
        writer.write(computeSimulation(algorithm, randomCHWithRandomSort, true, instanceName, variantNames.get(11)) + "");
    }

    /** Cette méthode permet de lancer une simulation sur un algorithme dotée d'une variante de configuration précise.
     *
     * @param algorithm Instance de l'algorithme à utiliser.
     * @param configuration Instance de la configuration de placement à utiliser.
     * @param randomBased Booléen, vaut True si la simulation repose sur de l'aléatoire, False sinon.
     * @param instanceName Nom du type de l'algorithme.
     * @param configurationName Nom du type de configuration de placement.
     *
     * @return Le score de simulation (+ écart-type si nécessaire).
     */
    private static String computeSimulation(Engine algorithm, PlacementConfiguration configuration, boolean randomBased, String instanceName, String configurationName){
        System.out.println("Is Running, please wait ... | " + instanceName + " --> " + algorithm.getClass().getSimpleName() + " --- " + configurationName);
        double sum = 0;
        double mean = 0;
        int currentScore = 0;
        ArrayList<Integer> values = new ArrayList<>(0);

        algorithm.setConfiguration(configuration);
        if(randomBased){
            for(int i = 0 ; i < nbSimulationForRandom ; i++){
                algorithm.reloadField();
                algorithm.computeBuildingPlacement();
                currentScore = algorithm.getScore();
                values.add(currentScore);
                sum += currentScore;
            }
            mean = sum / nbSimulationForRandom;
            return mean + " (" + formatter.format(computeStandardDeviation(values, mean)) + ")";
        }
        else{
            algorithm.reloadField();
            algorithm.computeBuildingPlacement();
            return String.valueOf(algorithm.getScore());
        }
    }

    /** Cette méthode permet de calculer l'écart-type d'une liste de valeurs entières.
     * <br>
     * Pour plus de rapidité, la moyenne de cette liste doit être fournie en paramètre.
     *
     * @param values La liste de valeur.
     * @param mean La moyenne des valeurs de cette liste.
     *
     * @return L'écart-type de cette liste.
     */
    private static double computeStandardDeviation(ArrayList<Integer> values, double mean){
        double sum = 0;
        for(Integer value : values){
            sum += Math.pow(Math.abs(value - mean), 2);
        }
        return Math.sqrt(sum/values.size());
    }
}