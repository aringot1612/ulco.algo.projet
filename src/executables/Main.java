package executables;

import core.Engine;
import core.algos.MaxArea;
import utilities.FileReader;
import utilities.PlacementConfiguration;

public class Main {
    public static void main(String[] args) {
        // INPUT Actuel -> Meilleur configuration
        // Pour plus d'informations concernant les input disponibles, Voir le fichier suivant : inputDetails.txt

        // INPUT 1
        FileReader fr = new FileReader("instances/instance03.dat");

        // INPUT 2
        PlacementConfiguration placementConfiguration = new PlacementConfiguration("middle", "distance", 1);

        // INPUT 3
        Engine engine = new MaxArea(fr, placementConfiguration);

        engine.computeBuildingPlacement();
        engine.printGameInfos();
    }
}