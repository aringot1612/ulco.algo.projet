package core;

import core.buildings.Building;
import core.buildings.CityHall;
import utilities.PlacementConfiguration;
import utilities.Pair;
import java.util.ArrayList;

/** Classe représentant un terrain. */
public class Field {
    /** Longueur du terrain. */
    private final int length;
    /** Largeur de terrain. */
    private final int width;
    /** Liste des bâtiments rattachés au terrain. */
    private final ArrayList<Building> buildings;
    /** Instance de configuration de placement. */
    private final PlacementConfiguration configuration;

    /** Constructeur
     *
     * @param length La longueur du terrain.
     * @param width La largeur du terrain.
     * @param configuration La configuration de placement pour notre terrain (permet de personnaliser l'emplacement de l'hôtel de ville et le triage des placements possibles).
     */
    public Field(int length, int width, PlacementConfiguration configuration){
        this.length = length;
        this.width = width;
        this.buildings = new ArrayList<>(0);
        this.configuration = configuration;
    }

    /** Permet d'afficher le terrain.
     * <br>
     * Ici, nous allons utiliser un tableau 2D juste pour l'affichage.
     * <br>
     * Ce tableau sera d'abord rempli de "Routes".
     * <br>
     * Ensuite, les cellules utilisées par un bâtiment seront mise à jour.
     */
    public void printField(){
        // Création d'au tableau 2D pour l'affichage.
        ArrayList<ArrayList<String>> fieldRepresentation = new ArrayList<>(0);
        // Ci-dessous, remplissage de notre tableau d'affichage, vide pour le moment.
        for(int i = 0; i < this.length ; i++){
            fieldRepresentation.add(new ArrayList<>(this.width));
            for(int j = 0; j < this.width ; j++)
                fieldRepresentation.get(i).add("R");
        }
        // Pour chaque bâtiment présent...
        for(Building building: this.buildings){
            // On parcourt les cellules occupés par le bâtiment concerné.
            for(int i = building.getXPosition() ; i < building.getXPosition() + building.getLength() ; i++){
                for(int j = building.getYPosition() ; j < building.getYPosition() + building.getWidth() ; j++){
                    // Chaque cellule correspondante prendra comme valeur : le symbole du bâtiment concerné.
                    fieldRepresentation.get(i).set(j, building.getSYMBOL());
                }
            }
        }
        // Ici, il s'agit de l'affichage console de notre tableau 2D.
        for(int i = 0; i < this.length ; i++){
            System.out.println();
            for(int j = 0; j < this.width ; j++){
                System.out.print(fieldRepresentation.get(i).get(j) + " ");
            }
        }
    }

    /** Cette méthode va tenter de placer un bâtiment.
     *
     * @param building Le bâtiment à placer.
     */
    public void tryPlaceBuilding(Building building){
        // On récupère tous les placements possibles pour ce bâtiment ici.
        ArrayList<Pair> possiblePlacements = findPossiblePlacements(building);
        // Si le bâtiment à placer n'est pas l'hôtel de ville...
        if(!this.buildings.isEmpty())
            // Triage des placements possible selon sa configuration.
            configuration.sortPlacements(possiblePlacements, (CityHall) this.buildings.get(0));
        // Si le placement est possible, on n'utilisera que le premier de la liste.
        if(possiblePlacements.size() != 0)
            placeBuilding(building, possiblePlacements.get(0));
    }

    /** Cette méthode se charge de placer un bâtiment cible dans le terrain.
     *
     * @param building Le bâtiment à placer.
     * @param possiblePlacements Le placement valide dans le terrain, pour ce bâtiment.
     */
    private void placeBuilding(Building building, Pair possiblePlacements){
        building.setXPosition(possiblePlacements.getLeft());
        building.setYPosition(possiblePlacements.getRight());
        this.buildings.add(building);
    }

    /** Cette méthode se charge de supprimer un bâtiment cible dans le terrain.
     *
     * @param building Le bâtiment à supprimer.
     */
    private void removeBuilding(Building building) {
        this.buildings.remove(building);
    }

    /** Cette méthode se charge de trouver les placements possibles dans le terrain pour un bâtiment donné.
     *
     * @param building Le bâtiment à placer.
     *
     * @return La liste des placements possibles sous la forme (x ; y).
     */
    private ArrayList<Pair> findPossiblePlacements(Building building){
        // Création d'une liste de placements possibles (équivalent à une liste de vecteur 2D).
        ArrayList<Pair> possiblePlacements = new ArrayList<>(0);
        // Si le terrain ne possède aucun bâtiment placé...
        if(this.buildings.size() == 0){
            // On sait qu'il s'agit alors de l'hôtel de ville à placer. Il suffit de retourner le placement selon la configuration.
            possiblePlacements.add(configuration.placeCityHall((CityHall) building, this));
            return possiblePlacements;
        }
        else{
            // On parcourt tout notre terrain "virtuellement".
            for(int i = 0; i < this.getLength(); i++){
                for(int j = 0; j < this.getWidth(); j++){
                    // Ici, on prends un placement (i ; j).
                    // * On cherche à vérifier que le placement sélectionné est possible pour ce bâtiment :
                    //* --> Le bâtiment ne dépasse pas les limites du terrain.
                    //* --> Le bâtiment n'entre pas en collision avec les autres bâtiments.
                    if(checkConflicts(i, j, building))
                        possiblePlacements.add(new Pair(i, j));
                }
            }
            // On cherche à valider chaque placement possible. (Voir question 4 du projet)
            validatePlacement(building, possiblePlacements);
        }
        // On retourne la liste des placements possibles.
        return possiblePlacements;
    }

    /** Cette méthode va vérifier les collisions et sorties de terrain pour un bâtiment.
     *
     * @param i L'éventuel placement en i (axe x) pour ce bâtiment sur le terrain.
     * @param j L'éventuel placement en j (axe y) pour ce bâtiment sur le terrain.
     * @param building Le bâtiment à placer.
     *
     * @return True si le placement est jugé valide, False sinon.
     */
    private boolean checkConflicts(int i, int j, Building building){
        // Index qui nous servira à parcourir la liste des bâtiments déjà placés sur le terrain.
        int x = 0;

        // Si le bâtiment ne sort pas du terrain...
        if(checkOutOfBound(i, j, building)){
            // On vérifie ici que notre bâtiment n'entre en collision avec un autre bâtiment deja présent.
            while (x < this.buildings.size() && checkCollision(i, j, building, this.buildings.get(x))){
                x++;
            }
            // Si x est égal au nombre de bâtiment, alors aucun bâtiment présent n'est entré en collision avec notre bâtiment à placer.
            return (x == this.buildings.size());
        }
        else
            return false;
    }

    /** Cette méthode permet de vérifier qu'un bâtiment à placer n'entre pas en collision avec un autre bâtiment déjà placé.
     * <br>
     * On s'assure simplement que :
     * <br>
     * - Le bâtiment à placer possède un point de départ positionné après l'extrémité inférieure droite de l'autre bâtiment...
     * <br>
     * OU
     * <br>
     * - Le bâtiment à placer possède une extrémité inférieure droite positionnée avant le point de départ de l'autre bâtiment...
     * <br>
     * Les deux conditions sont vérifiés sur les axes x OU y OU les deux.
     * <br>
     * - Il suffit que l'une de ces deux conditions soit vraie pour que les deux bâtiments ne soit pas en collision...
     *
     * @param i La coordonnée en x pour le bâtiment à placer.
     * @param j La coordonnée en y pour le bâtiment à placer.
     * @param building Le bâtiment à placer.
     * @param placedBuilding Le bâtiment déjà placé dans le terrain.
     *
     * @return True si les deux bâtiments n'entrent pas en collision, False sinon.
     */
    private boolean checkCollision(int i, int j, Building building, Building placedBuilding){
        if(i >= placedBuilding.getXPosition() + placedBuilding.getLength() || j >= placedBuilding.getYPosition() + placedBuilding.getWidth())
            return true;
        else return i + building.getLength() <= placedBuilding.getXPosition() || j + building.getWidth() <= placedBuilding.getYPosition();
    }

    /** Cette méthode permet de vérifier qu'un bâtiment cible ne "sort" pas du terrain si placé sur (i ; j).
     *
     * @param i La coordonnée en x pour le bâtiment à placer.
     * @param j La coordonnée en y pour le bâtiment à placer.
     * @param building Le bâtiment à placer.
     *
     * @return True si le bâtiment ne sort pas du terrain, False sinon.
     */
    private boolean checkOutOfBound(int i, int j, Building building){
        return (i + building.getLength() <= this.length && j + building.getWidth() <= this.width);
    }

    /** Cette méthode permet de valider une liste de placements pour un bâtiment en vérifiant la contrainte suivante :
     * <br>
     * Tous les bâtiments du terrain sont directement reliés à l'hôtel de ville par une route.
     *
     * @param building Le bâtiment que nous devons placer (temporairement) pour vérifier notre contrainte.
     * @param possiblePlacements La liste des placements possibles pour ce bâtiment.
     */
    private void validatePlacement(Building building, ArrayList<Pair> possiblePlacements){
        // Création d'une liste de placements secondaire, Elle nous sert à supprimer les coordonnées invalides après vérification de chemin.
        ArrayList<Pair> placementsToRemove = new ArrayList<>(0);
        for(Pair placement : possiblePlacements){ // Pour chaque placement valide...
            if(!pathFinder(building, placement)) // Si, à la suite de ce placement (effectué virtuellement), le terrain ne respecte pas les contraintes imposées (voir question 4)...
                placementsToRemove.add(placement); // Ajout de la coordonnée de placement à une liste de suppression.
        }
        // Une fois toutes les contraintes de terrains vérifiées, on supprime les déplacements interdits par contrainte.
        possiblePlacements.removeAll(placementsToRemove);
    }

    /** Méthode de recherche de chemin.
     * <br>
     * Cet algorithme permet de vérifier qua chaque bâtiment placé est bien relié à l'hôtel de ville par une route.
     * <br>
     * Après le placement d'un nouveau bâtiment dans le terrain,
     * <br>
     * Nous devons vérifier que CE bâtiment, et tous les autres bâtiments déjà placés, sont liés à l'hôtel de ville par une route.
     *
     * @param building Le nouveau bâtiment à placer.
     * @param placement Sa coordonnée de placement.
     *
     * @return True si ce bâtiment peut être placé sans problème (il respecte la contrainte et n'empêche aucun autre bâtiment de la respecter), False sinon.
     */
    private boolean pathFinder(Building building, Pair placement){
        // Entier utilisé pour vérifier le nombre de bâtiments liés à l'hôtel de ville.
        int linkedBuildingsNb = 0;
        // Entier utilisé pour vérifier le nombre de cases (/parcelles) parcouru par notre algorithme.
        int pileIndex = 0;
        // Pile de coordonnées : Chaque cellule correspond à une parcelle de terrain.
        ArrayList<Pair> pile = new ArrayList<>(0);

        // Première étape : On place le nouveau bâtiment (il sera supprimé bien entendu par la suite).
        placeBuilding(building, placement);

        for(Building myBuilding : this.buildings){ // Pour chaque bâtiment déjà placé...
            myBuilding.isLinked(false); // On le défini comme non lié (état par défaut).
        }

        // Récupération de l'hôtel de ville : bâtiment "central" pour l'algorithme.
        Building cityHall = buildings.get(0);

        // On s'intéresse ici exclusivement aux parcelles de terrains occupées par l'hôtel de ville.
        for(int i = cityHall.getXPosition() ; i < cityHall.getXPosition() + cityHall.getLength() ; i++){
            for(int j = cityHall.getYPosition() ; j < cityHall.getYPosition() + cityHall.getWidth() ; j++){
                // On recherche ici à ajouter les cases vides adjacentes à notre hotel de ville directement dans la pile.
                // On précise que les cases en questions sont proches de l'hôtel de ville.
                // Si un bâtiment est adjacent, il ne sera pas marqué.
                addToPile(i, j, pile, true);
            }
        }

        while (pileIndex < pile.size()){ // Tant que la pile n'a pas été totalement parcourue...
            Pair pair = pile.get(pileIndex); // Récupération de la prochaine coordonnée à vérifier.
            addToPile(pair.getLeft(), pair.getRight(), pile, false); // Vérification des cases adjacentes à cette coordonnée avec ajout à la pile si besoin.
            pileIndex++; // Permet de passer à la coordonnée suivante dans la pile.
        }

        // On atteint cette portion de code lorsque que tous les chemins partant de l'hôtel de ville sont vérifiés.
        for(Building myBuilding : this.buildings){ // Pour chaque bâtiment déjà placé...
            if(myBuilding.isLinked()) // Si ce bâtiment a été marqué tel que : lié à l'hôtel de ville...
                linkedBuildingsNb++; // Incrémentation du nombre de bâtiments liés.
        }
        // Suppression du bâtiment ajouté plus tôt.
        removeBuilding(building);
        // Si le nombre de bâtiment liés à l'hôtel de ville est égal au nombre de bâtiments présents + 1.
        // Cela signifie que TOUS les bâtiments + le prochain bâtiment à placer respectera la contrainte : Tous les bâtiments sont liés à l'hôtel de ville.
        return(linkedBuildingsNb == this.buildings.size()+1);
    }

    /** Cette méthode permet de vérifier et d'ajouter à une pile de coordonnées : Toute case vide adjacente à la coordonnée donnée en paramètre.
     * <br>
     * Cette méthode nous sert également à marquer les bâtiments détectés dans les cases adjacentes. <br>
     * Pour chaque case adjacente disponible. <br>
     *
     * - Si elle est existe dans notre terrain... <br>
     * ET <br>
     * - Si elle est vide... <br>
     * ET <br>
     * - Si elle n'a pas encore été ajoutée dans notre liste auparavant... <br>
     *
     * Alors La case adjacente en question est ajoutée dans la pile.
     * @param x La composante x de notre coordonnée.
     * @param y La composante y de notre coordonnée.
     * @param pile La pile de coordonnées.
     * @param isNearCityHall Un booléen précisant si la case vérifiée est collée à l'hôtel de ville (dans ce cas, le bâtiment ne peut être marqué directement).
     */
    private void addToPile(int x, int y, ArrayList<Pair> pile, boolean isNearCityHall){
        // Récupération des quatre cases (/parcelles) adjacentes à une case de coordonnée spécifique.
        Pair upper = new Pair(x-1, y);
        Pair left = new Pair(x, y-1);
        Pair bottom = new Pair(x+1, y);
        Pair right = new Pair(x, y+1);

        if(checkOutOfField(upper) && checkEmptyPlacement(upper, isNearCityHall) && !checkIfPairExist(pile, upper))
            pile.add(upper);
        if(checkOutOfField(left) && checkEmptyPlacement(left, isNearCityHall) && !checkIfPairExist(pile, left))
            pile.add(left);
        if(checkOutOfField(bottom) && checkEmptyPlacement(bottom, isNearCityHall) && !checkIfPairExist(pile, bottom))
            pile.add(bottom);
        if(checkOutOfField(right) && checkEmptyPlacement(right, isNearCityHall) && !checkIfPairExist(pile, right))
            pile.add(right);
    }

    /** Cette méthode permet de vérifier qu'une coordonnée de terrain spécifique existe bien dans notre terrain.
     *
     * @param pair Une paire de valeur : Coordonnée dans le terrain sur (x ; y) à vérifier.
     *
     * @return True si la coordonnée existe dans le terrain, False sinon.
     */
    private boolean checkOutOfField(Pair pair){
        return (pair.getLeft() < this.length && pair.getRight() < this.width && pair.getLeft() >= 0 && pair.getRight() >= 0);
    }

    /** Cette méthode permet de vérifier si une parcelle de terrain de coordonnée spécifique est occupée ou non.
     * <br>
     * Elle permet également de détecter un bâtiment placé sur une coordonnée de terrain spécifique et de le "marquer".
     *
     * @param pair Une paire de valeur : Coordonnée dans le terrain sur (x ; y).
     * @param isNearCityHall Un booléen précisant si la case vérifiée est collée à l'hôtel de ville (dans ce cas, le bâtiment ne peut être marqué directement).
     *
     * @return True si la parcelle est inoccupée (case "vide"), False sinon.
     */
    private boolean checkEmptyPlacement(Pair pair, boolean isNearCityHall){
        for(Building building : this.buildings){ // Pour chaque bâtiment présent sur le terrain...
            if(!checkIfPlacementIsUsed(pair, building)){ // Si la parcelle est occupée par ce bâtiment
                if(!isNearCityHall)
                    building.isLinked(true); // On en profite pour le "marquer" tel que : lié à l'hôtel de ville.
                return false;
            }
        }
        return true;
    }

    /** Cette méthode permet de vérifier si une parcelle de terrain n'est pas déjà utilisée par le bâtiment donné en paramètre.
     *
     * @param coordinate Une paire de valeur : Coordonnée dans le terrain sur (x ; y).
     * @param building On cherche à savoir si ce bâtiment spécifique occupe cette parcelle.
     *
     * @return True si le bâtiment N'occupe PAS cette parcelle, False sinon
     */
    private boolean checkIfPlacementIsUsed(Pair coordinate, Building building){
        if(coordinate.getLeft() >= building.getXPosition() + building.getLength() || coordinate.getRight() >= building.getYPosition() + building.getWidth())
            return true;
        else return coordinate.getLeft() + 1 <= building.getXPosition() || coordinate.getRight() + 1 <= building.getYPosition();
    }

    /** Cette méthode permet de vérifier si une paire de valeur sous la forme (x ; y) est déjà présente dans une liste de paires du même type.
     *
     * @param tab La liste de paire que nous souhaitons parcourir.
     * @param pair La paire à "trouver" dans notre liste.
     *
     * @return True si notre paire de existe bien dans notre liste, False sinon..
     */
    private boolean checkIfPairExist(ArrayList<Pair> tab, Pair pair){
        for(Pair val : tab){
            if(val.isEqualTo(pair)){
                return true;
            }
        }
        return false;
    }

    /** Cette méthode permet d'évaluer le terrain.
     * <br>
     * Il suffit de faire la somme de toutes les aires de bâtiments existants.
     *
     * @return le score atteint par le terrain.
     */
    public int evaluateField()
    {
        int nbCasesBatiments = 0;

        for(Building b : buildings)
        {
            nbCasesBatiments += b.getLength() * b.getWidth();
        }

        return nbCasesBatiments;
    }

    /** Cette méthode permet de compter le nombre de bâtiments présents dans le terrain.
     * <br>
     * Son but est purement graphique, l'hôtel de ville est ignoré.
     *
     * @return le nombre de bâtiments présents dans le terrain (-1 pour l'hôtel de ville)
     */
    public int countBuildings()
    {
        return buildings.size() -1;
    }

    /** Getter permettant de récupérer la longueur du terrain.
     *
     * @return La longueur du terrain.
     */
    public int getLength() {return this.length;}

    /** Getter permettant de récupérer la largeur du terrain.
     *
     * @return La largeur du terrain.
     */
    public int getWidth(){return this.width;}

    /** Getter permettant de récupérer la liste des bâtiments présents dans le terrain.
     *
     * @return La liste des bâtiments du terrain.
     */
    public ArrayList<Building> getBuildings() {return buildings;}
}