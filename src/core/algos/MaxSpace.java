package core.algos;

import core.Engine;
import core.buildings.Building;
import utilities.FileReader;
import utilities.PlacementConfiguration;

/**
 * Classe MaxSpace, héritée de Engine.
 * <br>
 * Cette classe permet d'exécuter l'algorithme glouton en plaçant en priorité les bâtiments d'encombrement maximal.
 */
public class MaxSpace extends Engine {
    /** Constructeur
     *
     * @param fr Une instance de classe FileReader contenant les données nécessaires au moteur de simulation.
     */
    public MaxSpace(FileReader fr) {
        super(fr, new PlacementConfiguration());
    }

    /** Constructeur
     *
     *
     * @param fr Une instance de classe FileReader contenant les données nécessaires au moteur de simulation.
     * @param configuration Une instance de classe PlacementConfiguration permettant de paramétrer la manière de placer les bâtiments.
     */
    public MaxSpace(FileReader fr, PlacementConfiguration configuration) {super(fr, configuration);}

    /** Cette méthode permet de lancer l'algorithme de placement. */
    @Override
    public void computeBuildingPlacement(){
        this.sort();
        for (Building building : buildings)
            this.field.tryPlaceBuilding(building);
    }

    /**
     * Cette méthode permet de trier les bâtiments à placer de telle sorte que :
     * <br>
     * Le premier bâtiment de la liste (this.buildings) soit le bâtiment possèdent l'encombrement le plus grand.
     */
    private void sort(){
        Building cityHall = this.buildings.remove(0);
        this.buildings.sort((c1, c2) -> {
            if (c1.getLength() + c1.getWidth() > c2.getLength() + c2.getWidth()) return -1;
            if (c1.getLength() + c1.getWidth() < c2.getLength() + c2.getWidth()) return 1;
            return 0;
        });
        this.buildings.add(0, cityHall);
    }
}
