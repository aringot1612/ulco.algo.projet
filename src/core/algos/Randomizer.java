package core.algos;

import core.Engine;
import core.buildings.Building;
import utilities.FileReader;
import utilities.PlacementConfiguration;

import java.util.Collections;

/**
 * Classe Randomizer, héritée de Engine.
 * <br>
 * Cette classe permet d'exécuter l'algorithme glouton en plaçant les bâtiments dans un ordre complétement aléatoire.
 */
public class Randomizer extends Engine {
    /** Constructeur
     *
     * @param fr Une instance de classe FileReader contenant les données nécessaires au moteur de simulation.
     */
    public Randomizer(FileReader fr) {
        super(fr, new PlacementConfiguration());
    }

    /** Constructeur
     *
     *
     * @param fr Une instance de classe FileReader contenant les données nécessaires au moteur de simulation.
     * @param configuration Une instance de classe PlacementConfiguration permettant de paramétrer la manière de placer les bâtiments.
     */
    public Randomizer(FileReader fr, PlacementConfiguration configuration) {super(fr, configuration);}

    /** Cette méthode permet de lancer l'algorithme de placement. */
    @Override
    public void computeBuildingPlacement(){
        this.sort();
        for (Building building : buildings)
            this.field.tryPlaceBuilding(building);
    }

    /**
     * Cette méthode permet de mélanger tous les bâtiments de la liste (this.buildings).
     * <br>
     * Ainsi, les bâtiments seront placés aléatoirement.
     */
    private void sort(){
        Building cityHall = this.buildings.remove(0);
        Collections.shuffle(this.buildings);
        this.buildings.add(0, cityHall);
    }
}
