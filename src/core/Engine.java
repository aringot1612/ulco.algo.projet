package core;

import core.buildings.Building;
import core.buildings.CityHall;
import utilities.PlacementConfiguration;
import utilities.FileReader;
import utilities.Pair;
import java.util.ArrayList;

/** Moteur de simulation.
 *<br>
 * Cette classe est à la fois le moteur de simulation principal et un algorithme à part entière.
 *<br>
 * Utiliser une instance de cette classe permet d'utiliser un algorithme glouton extrêmement naif.
 *<br>
 * Par défaut,
 * Ce dernier placera les bâtiments dans l'ordre des bâtiments récupérés à partir des instances.
 *<br>
 * Sans configuration supplémentaire,
 * Tous les bâtiments (dont l'hôtel de ville) seront placés en priorité dans le coin supérieur gauche du terrain.
 *
 * */
public class Engine {
    /** Nombre de bâtiment pour la simulation. */
    protected final int buildingsNb;
    /** Liste des bâtiments pour le moteur de simulation. */
    protected final ArrayList<Building> buildings;
    /** Instance de Terrain. */
    protected Field field;
    /** Instance de configuration de placement. */
    private PlacementConfiguration configuration;

    /** Constructeur
     *
     * @param fr Une instance de classe FileReader contenant les données nécessaires aux moteur de simulation.
     * @param configuration Une instance de classe PlacementConfiguration permettant de paramétrer la manière de placer les bâtiments.
     */
    public Engine(FileReader fr, PlacementConfiguration configuration){
        this.buildings = new ArrayList<>(0);
        // Conversion : Bâtiment sous forme de Paire <Longueur-Largeur> VERS Instance de Building.
        ArrayList<Pair<Integer, Integer>> buildings = fr.getBuildings();
        for(int i = 0 ; i < buildings.size() ; i++){
            this.buildings.add(createBuilding(i, buildings.get(i)));
        }
        this.buildingsNb = this.buildings.size();
        this.setConfiguration(configuration);
        // Création du terrain.
        this.field = new Field(fr.getLength(), fr.getWidth(), configuration);
    }

    /** Constructeur
     *
     * @param fr Une instance de classe FileReader contenant les données nécessaires aux moteur de simulation.
     */
    public Engine(FileReader fr){this(fr, new PlacementConfiguration());}

    /** Cette méthode permet de "vider" le terrain afin d'enchainer les simulations. */
    public void reloadField(){
        int length = this.field.getLength();
        int width = this.field.getWidth();
        this.field = new Field(length, width, this.configuration);
    }

    /** Permet de placer les bâtiments sur notre terrain en tenant compte des contraintes imposées. */
    public void computeBuildingPlacement(){
        for (Building building : this.buildings)
            this.field.tryPlaceBuilding(building);
    }

    /** Affiche les infos de simulation dans la console. */
    public void printGameInfos(){
        int index = 0;
        System.out.println("\nTaille du terrain : " + this.field.getLength() + " x " + this.field.getWidth());
        System.out.println("\nNombre de bâtiments : " + this.buildingsNb);
        for(Building building : this.buildings){ // Pour chaque bâtiment...
            if(index == 0) // Si il s'agit de l'hôtel de ville...
                System.out.println("\nDimensions de l'hôtel de ville : " + building.getLength() + " x " + building.getWidth()+"\n");
            else // Sinon...
                System.out.println("Dimensions du bâtiment " + index + " : " + building.getLength() + " x " + building.getWidth());
            index++;
        }
        System.out.println();
        this.field.printField();
        System.out.println("\nScore du terrain : " + this.field.evaluateField());
    }

    /** Cette méthode permet de créer un bâtiment à partir d'une paire (Longueur-Largeur).
     *
     * @param id L'identifiant du bâtiment à créer.
     * @param building Ses dimensions (Longueur ; Largeur).
     *
     * @return Le bâtiment créé.
     */
    private Building createBuilding(int id, Pair building){
        // Si l'identifiant du bâtiment est 0, il s'agit d'un hotel de ville (Bâtiment spécifique).
        if(id == 0)
            return new CityHall(building.getLeft(), building.getRight());
        else
            return new Building(id, building.getLeft(), building.getRight());
    }

    /** Getter retournant l'instance du terrain.
     *
     * @return le terrain actuel.
     */
    public Field getField() {return field;}

    /** Getter retournant le score du terrain.
     *
     * @return le score du terrain
     */
    public int getScore(){return this.field.evaluateField();}

    /** Setter pour mettre à jour la configuration.
     *
     * @param configuration La nouvelle configuration.
     */
    public void setConfiguration(PlacementConfiguration configuration){this.configuration = configuration;}
}