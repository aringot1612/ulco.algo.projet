package core.buildings;

/** Classe représentant un hotel de ville, héritée de Building. */
public class CityHall extends Building {
    /** Constructeur
     *
     * @param length La longueur du bâtiment.
     * @param width La largeur du bâtiment.
     */
    public CityHall(int length, int width){
        super(0, length, width);
        // Il s'agit d'un bâtiment spécifique, il aura donc droit à un symbole particulier : H
        this.SYMBOL = "H";
    }
}