package core.buildings;

/** Classe représentant un bâtiment. */
public class Building {
    /** Position en x dans le terrain. */
    protected int xPosition;
    /** Position en y dans le terrain. */
    protected int yPosition;
    /** Longueur du bâtiment. */
    protected final int length;
    /** Largeur du bâtiment. */
    protected final int width;
    /** Symbole permettant d'identifier le bâtiment. */
    protected String SYMBOL;
    /** Précise si ce bâtiment est relié à l'hôtel de ville ou non. */
    private boolean linked;

    /** Constructeur
     *
     * @param id L'identifiant du bâtiment.
     * @param length La longueur du bâtiment.
     * @param width La largeur du bâtiment.
     */
    public Building(int id, int length, int width){
        this.xPosition = -1;
        this.yPosition = -1;
        this.length = length;
        this.width = width;
        this.SYMBOL = String.valueOf(id);
    }

    /** Getter permettant de récupérer le symbole du bâtiment.
     *
     * @return Le symbole du bâtiment.
     */
    public String getSYMBOL(){return this.SYMBOL;}

    /** Getter permettant de récupérer la longueur du bâtiment.
     *
     * @return La longueur du bâtiment.
     */
    public int getLength() {return this.length;}

    /** Getter permettant de récupérer la largeur du bâtiment.
     *
     * @return La largeur du bâtiment.
     */
    public int getWidth() {return this.width;}

    /** Getter permettant de récupérer la position en x du bâtiment dans le terrain.
     *
     * @return La position en x du bâtiment.
     */
    public int getXPosition() {return this.xPosition;}

    /** Getter permettant de récupérer la position en y du bâtiment dans le terrain.
     *
     * @return La position en y du bâtiment.
     */
    public int getYPosition() {return this.yPosition;}

    /** Getter permettant de savoir si le bâtiment est lié ou non à l'hôtel de ville.
     *
     * @return True si le bâtiment est défini tel que : lié à l'hôtel de ville, False sinon.
     */
    public boolean isLinked() {return this.linked;}

    /** Setter permettant de mettre à jour la position en x du bâtiment dans le terrain.
     *
     * @param xPosition La nouvelle position en x du bâtiment.
     */
    public void setXPosition(int xPosition) {this.xPosition = xPosition;}

    /** Setter permettant de mettre à jour la position en y du bâtiment dans le terrain.
     *
     * @param yPosition La nouvelle position en y du bâtiment.
     */
    public void setYPosition(int yPosition) {this.yPosition = yPosition;}

    /** Setter permettant de définir si le bâtiment est lié à l'hôtel de ville ou non.
     *
     * @param linked Vaut True si le bâtiment est considéré tel que : lié à l'hôtel de ville, False sinon.
     */
    public void isLinked(boolean linked) {this.linked = linked;}
}