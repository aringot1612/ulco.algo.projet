package ui;

import core.Engine;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public class EvaluationBar extends HBox
{
    private Label label;
    private Engine engine;
    private static String suffix1 = " cells occupied by buildings, ";
    private static String suffix2 = " buildings";

    public EvaluationBar(Engine engine)
    {
        this.engine = engine;
        label = new Label(engine.getField().evaluateField() + suffix1 + engine.getField().countBuildings() + suffix2);
        setPadding(new Insets(10));
        setSpacing(5.0);
        getChildren().addAll(label);
    }

    public void update()
    {
        int nbCasesBuilding = engine.getField().evaluateField();
        int nbBuildings = engine.getField().countBuildings();
        label.setText(nbCasesBuilding + suffix1 + nbBuildings + suffix2);
    }

}
