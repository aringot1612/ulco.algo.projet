package ui;

import core.Engine;
import core.buildings.Building;
import core.buildings.CityHall;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class BuildingPane extends Pane
{
    private Engine engine;
    private ArrayList<Color> color;
    private double buildingRatio;

    public BuildingPane(int paneHeight, Engine engine)
    {
        this.engine = engine;

        color = new ArrayList<>();
        Collections.addAll(color, Color.RED, Color.ORANGE, Color.BROWN);

        engine.computeBuildingPlacement();
        getStyleClass().add("pane");

        buildingRatio = paneHeight / engine.getField().getLength();

        placeBuildingsOnScene();
    }

    private void placeBuildingsOnScene()
    {
        Rectangle rectangle;
        ArrayList<Building> buildings = engine.getField().getBuildings();

        for(Building b : buildings)
        {
            rectangle = new Rectangle(b.getYPosition()* buildingRatio, b.getXPosition()* buildingRatio, b.getWidth()* buildingRatio, b.getLength()* buildingRatio);
            getChildren().add(rectangle);
            if(b instanceof CityHall)
            {
                rectangle.getStyleClass().add("city_hall");
            }
            else
            {
                rectangle.getStyleClass().add("building");
                rectangle.setFill(color.get(new Random().nextInt(color.size())));
            }
        }
    }
}
