package utilities;

import core.Field;
import core.buildings.CityHall;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/** PlacementConfiguration
 * <br>
 * Cette classe permet de personnaliser nos algorithmes en y ajoutant des réglages précis.
 * <br>
 * Cette Classe de configuration permet de modifier deux choses, pouvant impacter grandement sur nos algorithmes :
 * <br>
 * - Le placement à utiliser par l'hôtel de ville. <br>
 * - Le triage des placements valides à effectuer avant de placer un bâtiment. <br>
 */
public class PlacementConfiguration {
    /** Type de placement pour l'hôtel de ville. */
    private final String cityHallPlacement;
    /** Type de triage pour les placements valides. */
    private final String sortPlacementType;
    /** Ordre de triage pour les placements valides */
    private final int sortOrder;
    /** Instance de Random (java.util). */
    private final Random random;

    /** Constructeur
     *
     * @param cityHallPlacement Le type de placement pour l'hôtel de ville.
     * @param sortPlacementType Le type de triage pour les placements valides.
     * @param sortOrder L'ordre de triage pour les placements valides.
     */
    public PlacementConfiguration(String cityHallPlacement, String sortPlacementType, int sortOrder){
        if(cityHallPlacement.equals("middle") || cityHallPlacement.equals("random") )
            this.cityHallPlacement = cityHallPlacement;
        else
            this.cityHallPlacement = "default";

        if(sortPlacementType.equals("distance") || sortPlacementType.equals("random"))
            this.sortPlacementType = sortPlacementType;
        else
            this.sortPlacementType = "default";

        if(sortOrder == 1 || sortOrder == -1)
            this.sortOrder = sortOrder;
        else
            this.sortOrder = 0;

        this.random = new Random();
    }

    /** Constructeur (default) */
    public PlacementConfiguration(){
        this("default", "default", 0);
    }

    /** Cette méthode permet de trier une liste de placement selon un type spécifique.
     *
     * @param placements La liste des placements à trier.
     * @param cityHall L'hôtel de ville (un type de triage doit connaitre l'hôtel de ville pour fonctionner).
     */
    public void sortPlacements(ArrayList<Pair> placements, CityHall cityHall){
        if(this.sortPlacementType.equals("distance"))
            sortPlacementsByDistance(placements, cityHall);
        else if (this.sortPlacementType.equals("random"))
            randomizePlacements(placements);
    }

    /** Cette méthode permet de trouver le placement pour l'hôtel de ville selon un type spécifique.
     *
     * @param cityHall L'hôtel de ville à placer.
     * @param field Le terrain sur lequel sera placé l'hôtel de ville.
     *
     * @return La paire de valeur (x;y) de placement pour l'hôtel de ville.
     */
    public Pair placeCityHall(CityHall cityHall, Field field){
        switch (this.cityHallPlacement) {
            case "middle":
                return new Pair(field.getLength() / 2 - cityHall.getLength() / 2, field.getWidth() / 2 - cityHall.getWidth() / 2);
            case "random":
                return new Pair(random.nextInt(field.getLength() - cityHall.getLength()), random.nextInt(field.getWidth() - cityHall.getWidth()));
            default:
                return new Pair(0, 0);
        }
    }

    /** Cette méthode permet de trier la liste des placement possibles par distance.
     * <br>
     * Ce triage spécifique cherche à calculer la distance séparant chaque placement de l'hôtel de ville.
     * <br>
     * Selon la valeur de sortOrder, le placement sera trié de manière croissante ou décroissante selon cette distance.
     * <br>
     * - sortOrder = 1, La plus grande distance entre le placement et l'hôtel de ville, en premier (décroissant).
     * <br>
     * - sortOrder = -1, La plus petite distance entre le placement et l'hôtel de ville, en premier (croissant).
     * <br>
     *
     * @param placements La liste des placements à trier.
     * @param cityHall L'hôtel de ville (nécessaire pour ce triage).
     */
    private void sortPlacementsByDistance(ArrayList<Pair> placements, CityHall cityHall){
        Pair cityHallCoordinate = new Pair((cityHall.getXPosition() + (cityHall.getLength() / 2)), (cityHall.getYPosition() + (cityHall.getWidth() / 2)));
        placements.sort((c1, c2) -> {
            if (calculateDistanceBetweenTwoPoints(c1, cityHallCoordinate) > calculateDistanceBetweenTwoPoints(c2, cityHallCoordinate))
                return -this.sortOrder;
            if (calculateDistanceBetweenTwoPoints(c1, cityHallCoordinate) < calculateDistanceBetweenTwoPoints(c2, cityHallCoordinate))
                return this.sortOrder;
            return 0;
        });
    }

    /** Cette méthode permet de trouver la distance séparant deux points.
     *
     * @param p1 Point 1.
     * @param p2 Point 2.
     *
     * @return La distance séparant les points 1 et 2.
     */
    private double calculateDistanceBetweenTwoPoints(Pair p1, Pair p2){
        return Math.sqrt(Math.pow(p2.getLeft() - p1.getLeft(), 2)+Math.pow(p2.getRight() - p1.getRight(), 2));
    }

    /** Cette méthode permet de mélanger les placements de manière aléatoire.
     *
     * @param placements La liste des placements à mélanger.
     */
    private void randomizePlacements(ArrayList<Pair> placements){
        Collections.shuffle(placements);
    }
}
