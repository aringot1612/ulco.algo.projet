package utilities;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Classe Utilitaire permettant d'ouvrir et de lire dans un fichier.
 * <br>
 * Cette classe nous permet de récupérer diverses informations pour le jeu :
 * <br>
 *
 * Dans l'ordre, <br>
 * - La longueur du terrain <br>
 * - La largeur du terrain <br>
 * - Le nombre de bâtiments <br>
 *
 * - la liste des bâtiments <br>
 * - - Pour chaque bâtiment présent, nous récupérons sa longueur et largeur <br>
 * - - Note : Le premier bâtiment de la ville correspond à l'hôtel de ville. <br>
 */
public class FileReader {
    /** Longueur du terrain. */
    private int length;
    /** Largeur du terrain. */
    private int width;
    /** Nombre de bâtiments. */
    private int buildingsNb;
    /** Liste de bâtiments sous formes de paires (Longueur-Largeur). */
    private final ArrayList<Pair<Integer, Integer>> buildings;

    /** Constructeur
     * @param path : Le chemin vers le fichier à lire.
     **/
    public FileReader(String path){
        this.length = 0;
        this.width = 0;
        this.buildingsNb = 0;
        this.buildings = new ArrayList<>();
        analyseFile(path);
    }

    /** Permet d'analyser un fichier dont le chemin est connu.
     *
     * @param customPath Chemin vers le fichier à ouvrir
     */
    public void analyseFile(String customPath){
        try {
            // Récupération du chemin de fichier.
            Path path = Paths.get(customPath);
            // Scanner java.
            Scanner scanner = new Scanner(path);
            // Si le fichier n'est pas vide/incorrect...
            if(scanner.hasNextLine()){
                // Tableau de String permettant de stocker des informations pré-traitement.
                String [] infos = scanner.nextLine().split(" ");
                // La longueur de terrain correspond à la première valeur de infos.
                length = Integer.parseInt(infos[0]);
                // La largeur de terrain correspond à la seconde valeur de infos.
                width = Integer.parseInt(infos[1]);
                // On récupère le nombre de bâtiments.
                buildingsNb = Integer.parseInt(scanner.nextLine());
                // Pour chaque bâtiment à récupérer...
                for(int i = 0 ; i < buildingsNb ; i++){
                    infos = scanner.nextLine().split(" ");
                    buildings.add(new Pair<>(Integer.parseInt(infos[0]), Integer.parseInt(infos[1])));
                }
            }
            // Fermeture du fichier.
            scanner.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    /** Getter permettant de récupérer la longueur du terrain.
     *
     * @return La longueur du terrain.
     */
    public int getLength() {return length;}

    /** Getter permettant de récupérer la largeur du terrain.
     *
     * @return La largeur du terrain.
     */
    public int getWidth() {return width;}

    /** Getter permettant de récupérer la liste des bâtiments.
     *
     * @return La liste des bâtiments.
     */
    public ArrayList<Pair<Integer, Integer>> getBuildings() {return buildings;}
}