package utilities;

/**
 * Classe Utilitaire permettant de gérer une paire de valeurs entières.
 */
public class Pair<L, R> {
    private final int left;
    private final int right;

    /** Constructeur
     *
     * @param left Valeur entière 1
     * @param right Valeur entière 2
     */
    public Pair(int left, int right) {
        this.left = left;
        this.right = right;
    }

    /** Getter permettant de récupérer la premiere valeur de notre paire.
     *
     * @return La première valeur de la paire.
     */
    public int getLeft() { return this.left; }

    /** Getter permettant de récupérer la seconde valeur de notre paire.
     *
     * @return La seconde valeur de la paire.
     */
    public int getRight() { return this.right; }

    /** Cette méthode permet de vérifier qu'une paire de valeur fournie en paramètre est égale ou non à la paire de valeur utilisée lors de l'appel.
     *
     * @param pair Paire de valeurs entières.
     * @return True si la paire utilisée pour l'appel de méthode est égale à la paire fournie en paramètre, False sinon.
     */
    public boolean isEqualTo(Pair pair){return (this.left == pair.getLeft() && this.right == pair.getRight());}
}