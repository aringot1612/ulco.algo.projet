# Algorithmique Avancée
## Projet : algorithmes gloutons pour le placement de bâtiments

## <br> Developpers

- [Arthur Ringot](https://gitlab.com/aringot1612)
- [Maxime Bulteel](https://gitlab.com/Mawime)

## <br> Langage choisi : Java

## <br> Comment utiliser le moteur de simulation :

Il est tout d'abord recommandé d'utiliser un IDE tel qu'**IntelliJ** pour ce projet.

Il est fortement conseillé d'utiliser le **jdk Java 1.8**.
En effet, ce dernier intègre directement la bibliothèque javafx, utilisée par l'affichage graphique pour l'un de nos programmes.

Les exécutables java sont situés dans le répertoire suivant : 
src/executables

Dans ce répertoire (qui est également un package java) se trouve trois fichiers distincts :

<br>

Le premier, **Main**, permet d'exécuter une simulation de placement avec affichage simplifié.

Dans le Main, on retrouve trois lignes permettant de personnaliser le moteur de simulation : **Input 1, Input 2 et input 3**. -> voir le fichier inputDetails.txt.
En fin de simulation, le terrain avec les bâtiments numérotés sont affichés, ainsi que le score correspondant.

Dans cet affichage console,
La lettre H désigne l'hôtel de ville et la lettre R désigne les routes.

<br>

Le second fichier s'appelle **MainFX**.

Tout comme le fichier Main, il est possible de personnaliser le programme grâce à 3 Inputs -> voir le fichier inputDetails.txt.

L'intérêt de ce fichier est qu'il propose une vue graphique du terrain.

Selon l'instance de données utilisée, la fenêtre graphique peut mettre un certain temps à apparaitre.

Dans cette fenêtre, l'hôtel de ville est représenté par un rectangle de couleur noir.
Les bâtiments sont représentés par des rectangles colorés.
Et enfin, les routes sont grisées.

En bas de fenêtre, le score du terrain ainsi que le nombre de bâtiments placés sont inscrits.

<br>

Le dernier fichier : **Simulation**

Ce dernier executable est utilisé pour générer les matrices de résultats associées à chaque instance dans des fichiers csv (accessibles dans le répertoire **/instances/**).

Il est tout à fait possible de ré-exécuter ce programme, ci-besoin, mais il peut être long : toutes les variantes d'algorithme sont testées, parfois plusieurs fois, et cela pour chaque instance.

## <br> Aperçu console :
<br>![Affichage console](images/consolePrint.png)

## <br> Aperçu graphique :
<br>![Affichage console](images/graphicalView.png)

## <br> Documentation

[Accessible ici](https://aringot1612.gitlab.io/ulco.algo.projet/)

## <br> Rapport de projet

[Accessible ici](https://aringot1612.gitlab.io/ulco.algo.projet/rapport.pdf)